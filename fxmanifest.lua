fx_version 'bodacious'

game 'gta5'

description 'ESX Moneywash script made by Kevin'

server_scripts {
    'config.lua',
    'server/main.lua'
}

client_script {
    'config.lua',
    'client/main.lua'
}